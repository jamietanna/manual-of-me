# Jamie Tanna's Manual of Me

Inspired by [Manual of Me](https://my.manualof.me/), this aims to provide a reference for my colleagues to understand how we can work together better.

This site is hosted at [manual.jvt.me](https://manual.jvt.me).
